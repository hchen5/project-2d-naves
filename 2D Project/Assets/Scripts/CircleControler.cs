using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CircleControler : MonoBehaviour
{
    
    [SerializeField]
	private float m_MoveSpeed = 2f;
	[SerializeField]
	private float m_JumpSpeed = 10f;

	//private bool m_onAir = true;
	private Rigidbody2D m_rigidBody;

	public Transform PuntoDisparo;
	public GameObject Bullet; 
	private Rigidbody2D bullet1;

	public static bool Final = false;
	
	 //Definim un delegat per avisar que he tocat un enemic
    public delegate void EnemyHit();
    public static event EnemyHit OnEnemyHit;


	void Awake()
	{
		print("m'he creat");
		m_rigidBody = GetComponent<Rigidbody2D>();
	}
	
    // Start is called before the first frame update
    void Start()
    {
		
        print("start, abans del primer frame");
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetKey(KeyCode.A))
		{
			//canvia els Angles del personatge
			transform.eulerAngles=new Vector3(0,180,0);
			m_rigidBody.velocity = new Vector3(-m_MoveSpeed,m_rigidBody.velocity.y,0);
			
		}
		if(Input.GetKey(KeyCode.D))
		{
			transform.eulerAngles=new Vector3(0,0,0);
			m_rigidBody.velocity = new Vector3(m_MoveSpeed,m_rigidBody.velocity.y,0);
			
		}
		if(Input.GetKeyUp(KeyCode.D) && !Input.GetKeyDown(KeyCode.W) && !Input.GetKeyDown(KeyCode.S) && !Input.GetKeyDown(KeyCode.A))
		{
			
			m_rigidBody.velocity = new Vector3(0,0,0);
			
		}
		if(Input.GetKeyUp(KeyCode.A) &&!Input.GetKeyUp(KeyCode.D) && !Input.GetKeyDown(KeyCode.W) && !Input.GetKeyDown(KeyCode.S))
		{
			m_rigidBody.velocity = new Vector3(0,0,0);
		
		}
		if(Input.GetKey(KeyCode.W))
		{
			transform.eulerAngles=new Vector3(0,180,90);
			m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,m_JumpSpeed,0);
			
		}
		if(Input.GetKeyUp(KeyCode.W) && !Input.GetKeyDown(KeyCode.S) && !Input.GetKeyDown(KeyCode.A) &&!Input.GetKeyUp(KeyCode.D))
		{
			
			m_rigidBody.velocity = new Vector3(0,0,0);
			
		}
		if(Input.GetKey(KeyCode.S))
		{
			transform.eulerAngles=new Vector3(0,180,-90);
			m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,-m_JumpSpeed,0);
			
		}
		
		if(Input.GetKeyUp(KeyCode.S) && !Input.GetKeyDown(KeyCode.A) &&!Input.GetKeyUp(KeyCode.D) &&!Input.GetKeyUp(KeyCode.W) )
		{
			
			m_rigidBody.velocity = new Vector3(0,0,0);
			
		}
		if(Input.GetKeyDown(KeyCode.Space)){
			Instantiate(Bullet,PuntoDisparo.position,PuntoDisparo.rotation);
		}
		/*
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			
			
				Instantiate(Bullet,PuntoDisparo.position,PuntoDisparo.rotation);
			}
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			
				Instantiate(Bullet,PuntoDisparo.position,PuntoDisparo.rotation);
			}
		if(Input.GetKeyDown(KeyCode.DownArrow)){
				Instantiate(Bullet,PuntoDisparo.position,PuntoDisparo.rotation);
			}
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			
			
				Instantiate(Bullet,PuntoDisparo.position,PuntoDisparo.rotation);
			}	*/


		//if(!m_onAir && 
		//	(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W)))
		//{
		//	m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,m_JumpSpeed,0);;
		//}
		
    }

	void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("TRIGGER: La pilota ha entrat a una col·lisió amb " + col.gameObject);

		OnEnemyHit.Invoke();
		Destroy(this.gameObject);
		//SceneManager.LoadScene("GUIGameOver");
    }
	
	/*
	void Movimiento(){
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			bullet1.velocity=new Vector2(m_MoveSpeed ,2f	)	;
		}
	}
	*/
}
