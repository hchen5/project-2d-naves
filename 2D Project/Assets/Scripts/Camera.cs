using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour{
public GameObject GameObject;
private Vector3 posicionRelativa;


// Use this for initialization
void Start () {

posicionRelativa = transform.position - GameObject.transform.position;

}

// Update is called once per frame
void LateUpdate () {

transform.position = GameObject.transform.position + posicionRelativa;

}

}