using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boto : MonoBehaviour
{
    public delegate void CLickButton();
    public static event CLickButton OnClickButton;

    // Start is called before the first frame update
    
    public void Reintentar() {
        OnClickButton.Invoke();
    }
}
