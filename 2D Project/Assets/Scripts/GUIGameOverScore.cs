using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class GUIGameOverScore : MonoBehaviour
{
    int score = GameManager.Instance.Score - 1;
    private void Awake()
    {
        
        print("la puntuaci s " + GameManager.Instance.Score);
        GetComponent<TextMeshProUGUI>().text = "Score: "+score.ToString();
    }

}
