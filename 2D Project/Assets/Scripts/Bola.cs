using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour
{

    private Rigidbody2D bola;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        bola = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bola.velocity = transform.right * Speed;

        Destroy(gameObject, 2f);
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("COLLISION: La pilota ha entrat a una col�lisi� amb " + col.gameObject);
        if (col.gameObject.tag == "Terra")
            Destroy(this.gameObject);
    }

}
