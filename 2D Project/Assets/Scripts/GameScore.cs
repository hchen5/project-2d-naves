using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScore : MonoBehaviour
{
    public static int Score;
    public string ScoreString = "Score";

    public Text TextScore;
    public static GameScore Gamescore;

    void awake()
    {
        Gamescore = this;
    }
    void Start()
    {
        Score = 0;
        TextScore.text = "Score: " + Score;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag=="Circle")
        {
            Score += 1;
            
        }
        
    }

    void Update()
    {
        TextScore.text = "Score: " + Score;
    }
}
