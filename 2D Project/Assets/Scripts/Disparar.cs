using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    private Rigidbody2D bullet1;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        bullet1=GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        bullet1.velocity=transform.right * Speed;
        
        Destroy(gameObject,2f);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("TRIGGER: La pilota ha entrat a una col·lisió amb " + col.gameObject);
		Destroy(col.gameObject);
        if (col.gameObject.tag == "Circle")
            Destroy(this.gameObject);
        GameScore.Score += 1;
        GameManager.Instance.Score++;
    }
   
}
